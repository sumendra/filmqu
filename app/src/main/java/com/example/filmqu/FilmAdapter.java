package com.example.filmqu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FilmAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Film> films;

    public FilmAdapter(Context context) {
        this.context = context;
        films= new ArrayList<>();
    }

    public void setFilms(ArrayList<Film> films) {
        this.films = films;
    }

    @Override
    public int getCount() {
        return films.size();
    }

    @Override
    public Object getItem(int position) {
        return films.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        }
        ViewHolder viewHolder= new ViewHolder(convertView);
        Film film= (Film) getItem(position);
        viewHolder.bind(film);
        return convertView;
    }
    private class ViewHolder{
        private TextView tvJudul;
        private TextView tvDeskripsi;
        private ImageView imagePhoto;
        ViewHolder(View view){
            tvJudul= view.findViewById(R.id.text_judul);
            tvDeskripsi= view.findViewById(R.id.text_deskripsi);
            imagePhoto= view.findViewById(R.id.image_photo);
        }
        void bind(Film film){
            tvJudul.setText(film.getJudul());
            tvDeskripsi.setText(film.getDeskripsi());
            imagePhoto.setImageResource(film.getPhoto());
        }
    }
}
<<<<<<< HEAD

=======
>>>>>>> 6af3d65c63e15ec9c0544395975746728efaeae0
