package com.example.filmqu;

<<<<<<< HEAD
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

=======
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
>>>>>>> 6af3d65c63e15ec9c0544395975746728efaeae0
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    Film film;
    TextView textJudul;
    TextView textDeskription;
    ImageView imagePhoto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        film=getIntent().getParcelableExtra("film");
        textJudul= findViewById(R.id.text_judul);
        textJudul.setText(film.getJudul());
        textDeskription= findViewById(R.id.text_deskripsi);
        textDeskription.setText(film.getDeskripsi());
        imagePhoto= findViewById(R.id.image_photo);
        imagePhoto.setImageResource(film.getPhoto());
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }
}
