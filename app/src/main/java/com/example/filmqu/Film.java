package com.example.filmqu;

import android.os.Parcel;
import android.os.Parcelable;

public class Film implements Parcelable {
    private int photo;
    private String judul;
    private String deskripsi;

    public Film(int photo, String judul, String deskripsi) {
        this.photo = photo;
        this.judul = judul;
        this.deskripsi = deskripsi;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public Film() {
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.photo);
        dest.writeString(this.judul);
        dest.writeString(this.deskripsi);
    }

    protected Film(Parcel in) {
        this.photo = in.readInt();
        this.judul = in.readString();
        this.deskripsi = in.readString();
    }

    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel source) {
            return new Film(source);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };
}
<<<<<<< HEAD

=======
>>>>>>> 6af3d65c63e15ec9c0544395975746728efaeae0
