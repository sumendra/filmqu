package com.example.filmqu;

<<<<<<< HEAD
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.TypedArray;
=======
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
>>>>>>> 6af3d65c63e15ec9c0544395975746728efaeae0
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
<<<<<<< HEAD
=======
import android.widget.Toast;
>>>>>>> 6af3d65c63e15ec9c0544395975746728efaeae0

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textJudul;
    TextView textDeskription;
    ImageView imagePhoto;
    ListView listView;
    ArrayList<Film> film;
    FilmAdapter adapter;
    String dataFilm[];
    String dataDeskripsi[];
    TypedArray dataPhoto;
    Film filmFavorit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        film= new ArrayList<>();
        textJudul= findViewById(R.id.text_judul);
        textDeskription= findViewById(R.id.text_deskripsi);
        imagePhoto= findViewById(R.id.image_photo);
        listView= findViewById(R.id.listview);
        adapter= new FilmAdapter(this);
        listView.setAdapter(adapter);
        convert();
        addItem();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IntentToFile(position);
            }
        });
    }
    private void addItem(){
        film= new ArrayList<>();
        for (int i = 0; i <dataFilm.length ; i++) {
            filmFavorit= new Film();
            filmFavorit.setJudul(dataFilm[i]);
            filmFavorit.setDeskripsi(dataDeskripsi[i]);
            filmFavorit.setPhoto(dataPhoto.getResourceId(i, -1));
            film.add(filmFavorit);
        }
        adapter.setFilms(film);
    }
    private void convert(){
        dataFilm= getResources().getStringArray(R.array.nama_film);
        dataDeskripsi= getResources().getStringArray(R.array.deskripsi_film);
        dataPhoto= getResources().obtainTypedArray(R.array.data_photo);
    }
    private void IntentToFile(int postion){
        Intent moveObjectIntent= new Intent(MainActivity.this, DetailActivity.class);
        moveObjectIntent.putExtra("film", film.get(postion));
        startActivity(moveObjectIntent);
    }
}
